//
//  ContentView.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI

struct WelcomeView: View {
    
    @EnvironmentObject var router: WelcomeRouter
    
    var body: some View {
        ZStack {
            Color.ggDefaultBG.ignoresSafeArea()
            VStack(spacing: .tuned(16)) {
                Spacer()
                Text("Welcome to BuddyGG!")
                    .font(.ggLargeTitle)
                    .foregroundStyle(.white)
                Text("Discover new friends and exciting games. \nJoin our community and start playing together")
                    .font(.ggLargeBody)
                GGButton(type: .primary, title: "Create an account", disabled: false) {
                    router.navigate(to: .signUp)
                }
                
                GGButton(type: .secondary, title: "Sign in with email", disabled: false) {
                    router.navigate(to: .signIn)
                }
                    
            }.padding(.horizontal, .tuned(16))
        }
    }
}

#Preview {
    WelcomeView()
}
