//
//  GGTextField.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 10.06.2024.
//

import SwiftUI

struct GGTextField: View {
    
    private let title: String
    private var text: Binding<String>
    private let isSecure: Bool
    private let errorText: String
    @ScaledMetric(relativeTo: .body) private var secureFieldHeight: CGFloat = 21
    @ScaledMetric(relativeTo: .body) private var secureFieldFontSize = CGFloat(17)
    
    init(title: String, text: Binding<String>, isSecure: Bool? = false, errorText: String = "") {
        self.title = title
        self.text = text
        self.isSecure = isSecure ?? false
        self.errorText = errorText
    }
    
    var body: some View {
        ZStack {
            Group {
                if isSecure {
                    SecureField("", text: text)
                       
                    
                } else {
                    TextField("", text: text)
                }
            } 
            .frame(height: 44)
            .padding(.horizontal, 16)
                .font(.ggLargeBody)
                .background(Color.ggDarkGray)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(self.errorText.isEmpty == true ? .clear : .ggRed, lineWidth: 1))
                

                       
        
            HStack {
                Text(title)
                    .font(text.wrappedValue.isEmpty ? .ggLargeBody : .ggBody)
                    .foregroundColor(text.wrappedValue.isEmpty ? .ggLightGray : .white)
                    .offset(x: text.wrappedValue.isEmpty ? 16 : 0, y: text.wrappedValue.isEmpty ? 0 : -35)
                    .transition(.slide)
                    .transition(.scale)
            Spacer()
            }.animation(.easeIn(duration: 0.1), value: text.wrappedValue)
                .allowsHitTesting(false)
        }.padding(.top, 20)
        if !self.errorText.isEmpty {
            HStack {
                Text(errorText).foregroundColor(.ggRed)
                Spacer()
            }
        }
       
       
    }
}

#Preview {
    GGTextField(title: "12312", text: .constant("1"), errorText: "")
}
