//
//  Gradients.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI

class Gradients {
    
    @ViewBuilder
    static var bg1: some View {
        GeometryReader { geo in
            self.bg1(radius: geo.size.height)
        }
    }
    
    @ViewBuilder
    private static func bg1(radius: CGFloat) -> some View {
        RadialGradient(colors: [Color(red: 33 / 255, green: 33 / 255, blue: 33 / 255),  // #212121
                                Color(red: 20 / 255, green: 20 / 255, blue: 20 / 255)], // #141414
                       center: .top,
                       startRadius: 0,
                       endRadius: radius)
    }
    
}
