//
//  Colors.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI

extension Color {
    
    // MARK: Background
    static var ggDefaultBG = Color(red: 19/255, green: 20/255, blue: 26/255) // #13141A
    
    // MARK: Colors
    static var ggPurple = Color(red: 93/255, green: 5/255, blue: 230/255) // #5D05E6
    static var ggBlue = Color(red: 5/255, green: 61/255, blue: 230/255) // #053DE6
    static var ggBlack = Color(red: 19/255, green: 20/255, blue: 26/255) // #13141A
    static var ggDarkGray = Color(red: 32/255, green: 34/255, blue: 43/255) // #20222B
    static var ggLightGray = Color(red: 99/255, green: 100/255, blue: 107/255) // #63646B
    static var ggRed = Color(red: 236/255, green: 36/255, blue: 36/255) // #EC2424
    static var ggGreen = Color(red: 96/255, green: 210/255, blue: 108/255) // #60D26C
    static var ggLightPurple = Color(red: 184/255, green: 197/255, blue: 236/255) // #B8C5EC
    
}
