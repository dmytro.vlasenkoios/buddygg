//
//  GGButton.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 09.06.2024.
//

import SwiftUI


// MARK: - ButtonType
enum ButtonType {
    case primary, secondary, tertiary
}

struct GGButton: View {
    
    
    init(type: ButtonType, title: String, disabled: Bool, buttonAction: (() -> Void)?) {
        self.type = type
        self.title = title
        self.disabled = disabled
        self.buttonAction = buttonAction
    }
    
    private let type: ButtonType
    private let title: String
    private var disabled: Bool
    private let buttonAction: (() -> Void)?
    
    var body: some View {
        Button(action: {
            buttonAction?()
        }, label: {
            Text(title).padding()
                .frame(height: .tuned(44))
                .frame(maxWidth: .infinity)
        })
        .buttonStyle(GGButtonStyle(type: self.type, disabled: self.disabled))
        .cornerRadius(.tuned(10))
        .if(self.type == .tertiary, transform: {
            $0.overlay {
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.ggPurple, lineWidth: 1)
            }
        })
        .disabled(self.disabled)
    }
    

}

struct GGButtonStyle: ButtonStyle {
    
    let type: ButtonType
    let disabled: Bool
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .foregroundColor(colorForType())
            .background(backgroundView)
            .opacity(opacityForState(isPressed: configuration.isPressed))
            .contentShape(Rectangle())
    }
    
    @ViewBuilder
    private var backgroundView: some View {
        switch self.type {
        case .primary:
            Color.ggPurple
        case .secondary:
            Color.ggLightPurple
        case .tertiary:
            Color.clear
        }
    }
    
    private func opacityForState(isPressed: Bool) -> Double {
        if isPressed {
            return 0.8
        }
        return 1
    }
    
    private func colorForType() -> Color {
        switch (self.type, self.disabled) {
        case (.primary, false):
            Color.white
        case (.primary, true):
            Color.white.opacity(0.5)
        case (.secondary, false), (.tertiary, false):
            Color.ggPurple
        case (.secondary, true), (.tertiary, true):
            Color.ggPurple.opacity(0.5)
        }
    }
}

#Preview {
    VStack {
        GGButton(type: .primary, title: "Primary", disabled: false, buttonAction: nil)
        GGButton(type: .secondary, title: "Secondary", disabled: false, buttonAction: nil)
        GGButton(type: .tertiary, title: "Tertiary", disabled: false, buttonAction: nil)
        Rectangle().frame(height: 200).opacity(0)
        GGButton(type: .primary, title: "Primary", disabled: true, buttonAction: nil)
        GGButton(type: .secondary, title: "Secondary", disabled: true, buttonAction: nil)
        GGButton(type: .tertiary, title: "Tertiary", disabled: true, buttonAction: nil)
    }.padding(.horizontal, 16)
   
}
