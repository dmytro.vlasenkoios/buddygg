//
//  BuddyGGApp.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI
import FirebaseCore

class AppDelegate: NSObject, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    FirebaseApp.configure()
    return true
  }
}



@main
struct BuddyGGApp: App {
  // register app delegate for Firebase setup
  @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
  @ObservedObject var router = WelcomeRouter()

  var body: some Scene {
    WindowGroup {
        NavigationStack(path: $router.navPath) {
            WelcomeView()
                .navigationDestination(for: WelcomeRouter.Destination.self) { destination in
                switch destination {
                case .signIn:
                    SignInView()
                case .signUp:
                    Rectangle().foregroundColor(.blue)
                case .restore:
                    Rectangle().foregroundColor(.green)
                }
            }
        }
        .environmentObject(router)
    }
  }
}
