//
//  Container.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import Swinject
import SwinjectStoryboard



extension SwinjectStoryboard {

    class func setup() {
        Container.loggingFunction = { /*logger.verbose($0)*/ _ in () }
        
        
        // add other module's DI's init here
    }
}

struct DIContainer {
    
    // MARK: - Singleton
    static let central = SwinjectStoryboard.defaultContainer
    private init() {}
}

extension Container {
    func inject<T>(name: String? = nil) -> T? {
        self.synchronize().resolve(T.self, name: name)
    }
}
