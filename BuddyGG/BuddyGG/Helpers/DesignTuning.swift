//
//  DesignTuning.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 09.06.2024.
//

import Foundation
import UIKit

enum ScaleSize: CGFloat {
    case large = 1.1
    case medium = 1
    case small = 0.95
    case xSmall = 0.8

    var isSmall: Bool {
        [.small, .xSmall].contains(self)
    }

    fileprivate init(forScreenWithHeight height: CGFloat) {
        switch height {
        case 428...: self = .large
        case 390..<428: self = .medium
        case 375..<390: self = .small
        case 0..<375: self = .xSmall

        default: fatalError()
        }
    }
}

struct DesignTuning {

    static func computeScale() {
        _ = tuningScale
    }

    static let tuningScaleSize: ScaleSize = {
        let screen = UIScreen.main
        let minBound = min(screen.bounds.width, screen.bounds.height)
        return ScaleSize(forScreenWithHeight: minBound)
    }()
    
    static let isSmallWidth: Bool = {
        // include SE, but NOT X, Xs
        let screen = UIScreen.main
        return max(screen.bounds.width, screen.bounds.height) < 800
    }()

    fileprivate static let tuningScale: CGFloat = {
        Self.tuningScaleSize.rawValue
    }()
}

extension CGFloat {

    static func tuned(_ value: CGFloat) -> CGFloat {
        value * DesignTuning.tuningScale
    }

    static func tunedRounded(_ value: CGFloat) -> CGFloat {
        tuned(value).rounded(.up)
    }
}
