//
//  ConditionalModifier.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 09.06.2024.
//

import SwiftUI

extension View {
    /// Applies the given transform if the given condition evaluates to `true`.
    /// - Parameters:
    ///   - condition: The condition to evaluate.
    ///   - transform: The transform to apply to the source `View`.
    /// - Returns: Either the original `View` or the modified `View` if the condition is `true`.
    @ViewBuilder func `if`<Content: View>(
        _ condition: @autoclosure () -> Bool,
        transform: (Self) -> Content
    ) -> some View {
        if condition() {
            transform(self)
        } else {
            self
        }
    }
}

extension View {
    /// Applies the given transform if the given `optionalValue` is not `nil`
    /// - Parameters:
    ///   - optionalValue: The value to unwrap.
    ///   - transform: The transform to apply to the source `View`.
    /// - Returns: Either the original `View` or the modified `View` if the `optionalValue` is non-nil and the transform can be aplied
    @ViewBuilder func iflet<Content: View, T>(
        _ optionalValue: @autoclosure () -> Optional<T>,
        transform: (Self, T) -> Content
    ) -> some View {
        if let unwrapped = optionalValue() {
            transform(self, unwrapped)
        } else {
            self
        }
    }
}
