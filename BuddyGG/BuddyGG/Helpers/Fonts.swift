//
//  Fonts.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 09.06.2024.
//

import UIKit
import SwiftUI

// MARK: - Font + SwiftUI
extension View {
    
    func font(_ info: FontInfo, tuned: Bool = true) -> some View {
        self.font(.system(info: info, tuned: tuned))
    }
}

extension Text {
    func font(_ info: FontInfo, tuned: Bool = true) -> Text {
        self.font(.system(info: info, tuned: tuned))
    }
}

extension Font {
    
    static func system(info: FontInfo, tuned: Bool = true) -> Font {
        .system(
            size: tuned ? .tunedRounded(info.size) : info.size,
            weight: info.weight
        )
    }
}

struct FontInfo {
    let size: CGFloat
    let weight: Font.Weight
}



extension FontInfo {
    
    static var ggLargeTitle: FontInfo {
        FontInfo(size: 28, weight: .bold)
    }
    
    static var ggTitle: FontInfo {
        FontInfo(size: 21, weight: .bold)
    }
    
    static var ggSmallTitle: FontInfo {
        FontInfo(size: 16, weight: .bold)
    }
    
    static var ggSubtitle: FontInfo {
        FontInfo(size: 14, weight: .bold)
    }
    
    static var ggLargeBody: FontInfo {
        FontInfo(size: 16, weight: .medium)
    }
    
    static var ggBody: FontInfo {
        FontInfo(size: 14, weight: .medium)
    }
    
    static var ggSmallBody: FontInfo {
        FontInfo(size: 14, weight: .regular)
    }
    
    static var ggMediumCaption: FontInfo {
        FontInfo(size: 12, weight: .medium)
    }
    
    static var ggRegularCaption: FontInfo {
        FontInfo(size: 12, weight: .regular)
    }
    
}
