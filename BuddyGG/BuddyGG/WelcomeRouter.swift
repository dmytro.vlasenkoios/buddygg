//
//  MainRouter.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI

final class WelcomeRouter: ObservableObject {
    
    public enum Destination: Codable, Hashable {
        case signIn
        case signUp
        case restore
    }
    
    @Published var navPath = NavigationPath()
    
    func navigate(to destination: Destination) {
        navPath.append(destination)
    }
    
    func navigateBack() {
        navPath.removeLast()
    }
    
    func navigateToRoot() {
        navPath.removeLast(navPath.count)
    }
}
