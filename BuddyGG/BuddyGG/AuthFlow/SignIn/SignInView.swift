//
//  SignInView.swift
//  BuddyGG
//
//  Created by Dmytro Vlasenko on 08.06.2024.
//

import SwiftUI
import Swinject


struct SignInView: View {
    
    @EnvironmentObject var router: WelcomeRouter
    @State var emailText: String = ""
    @State var passowrdText: String = ""
    
    
    
    var body: some View {
        ZStack {
            Color.ggDefaultBG.ignoresSafeArea()
            VStack(spacing: .tuned(16)) {
        
                Text("Welcome back!")
                    .font(.ggLargeTitle)
                    .foregroundStyle(.white)
                                    GGTextField(title: "Enter your email", text: $emailText)
                                    GGTextField(title: "Enter your password", text: $passowrdText, isSecure: true)
                    
                
               
                Spacer()
                GGButton(type: .primary, title: "Sign in", disabled: false) {
                    
                }
                
                GGButton(type: .secondary, title: "Back", disabled: false) {
                    router.navigateToRoot()
                }
               
            }.padding(.horizontal, .tuned(16))
        }.navigationBarBackButtonHidden(true)
    }
}

#Preview {
    SignInView()
}
